package net.smartyapps.admobintegration;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by mcatal on 14.05.2015.
 */
public class AdmobHelper {
    private static InterstitialAd interstitial;

    private static Context mContext;

    private static AdRequest adRequest;

    public static void showAds() {
        if (interstitial != null && interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    public static void initAds(Context context, String addUnitId) {
        mContext = context;

        interstitial = new InterstitialAd(context);

        interstitial.setAdUnitId(addUnitId);

        if (BuildConfig.DEBUG) {
            adRequest = new AdRequest.Builder().addTestDevice(
                    "8C77878BE8703BA7C9B4013A560AD6B3").build();
        } else {
            adRequest = new AdRequest.Builder().build();
        }

        interstitial.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();

                refreshAds();

                Toast.makeText(mContext, "AdClosed", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();

                Toast.makeText(mContext, "AdOpened", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();

                Toast.makeText(mContext, "AdLoaded", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();

                Toast.makeText(mContext, "AdLeftApp", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);

                Toast.makeText(mContext, "AdFailedToLoad", Toast.LENGTH_LONG).show();
            }
        });

        interstitial.loadAd(adRequest);
    }

    private static void refreshAds() {
        if (interstitial != null && !interstitial.isLoaded()) {

            if (BuildConfig.DEBUG) {
                adRequest = new AdRequest.Builder().addTestDevice(
                        "8C77878BE8703BA7C9B4013A560AD6B3").build();
            } else {
                adRequest = new AdRequest.Builder().build();
            }

            interstitial.loadAd(adRequest);
        }
    }
}
