package net.smartyapps.admobintegration;

import android.content.Context;

/**
 * Created by mcatal on 14.05.2015.
 */
public class Util {
    public static String getStringValueFromResource(Context context,int resourceId) {
        return context.getResources().getString(resourceId);
    }
}
